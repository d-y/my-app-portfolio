
/*
 * Copyright (c) 2015. Dennis Bing Yu All Rights Reserved
 */

package com.example.android.myappportfolio;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String createMsgString(int stringId) {
        return String.format(getString(R.string.msg_format_string), getString(stringId));
    }
    public void launchPortfolioApp(View view) {
        int id = view.getId();
        String msg = "";

        switch (id) {
            case R.id.button_spotify:
                msg = createMsgString(R.string.msg_format_string_spotify);
                break;
            case R.id.button_scores:
                msg = createMsgString(R.string.msg_format_string_scores);
                break;
            case R.id.button_library:
                msg = createMsgString(R.string.msg_format_string_library);
                break;
            case R.id.button_build_it_bigger:
                msg = createMsgString(R.string.msg_format_string_build_it_bigger);
                break;
            case R.id.button_bacon_reader:
                msg = createMsgString(R.string.msg_format_string_bacon_reader);
                break;
            case R.id.button_capstone:
                msg = createMsgString(R.string.msg_format_string_capstone);
                break;
        }

        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
